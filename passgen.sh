#!/bin/sh
#
# Wrapper around apg to generate various random strings.

die() {
    printf -- "%s\n" "$*" >&2
}

# Generate a single, seven characters, random user name.
username() {
    which apg > /dev/null || die "apg not installed!"
    apg -a0 -n1 -ML -m7 -x7
}

# Generate a single, sixteen characters, random password.
# If argument 'lame' is given, will generate a password without
# special characters.
password() {
    which apg > /dev/null || die "apg not installed!"
    mode=SNCL
    test "$1" = "lame" && mode=NCL
    apg -a1 -n1 -M"$mode" -m16 -x16
}

# Generate the answer to a security question.
# Not too long, easy to pronounce over the phone.
secquestion() {
    which apg > /dev/null || die "apg not installed!"
    apg -a1 -n1 -MNL -m12 -x12 | sed 's/..../& /g'
}

pin() {
    which apg > /dev/null || die "apg not installed!"
    apg -a1 -n1 -MN -m8 -x12
}
